Project name: Restaurant Operations

Description: This project deals with handling multiple endpoints to create, manage, update, delete (Menu lists, Customer Order and Total Invoice)

As part of Menu Creation:
- Menu Id
- Name of the Menu
- Spicy - yes or no
- Veg or Non Veg
- Type of Menu: Starter/Main Course/Deserts/
- Price of the menu will be listed


As part of Placing Orders:
- Order No {Unique for every orders}
- Order id 
- Name of the Menu
- Quantitiy of the order
- Customer firstname
- Customer Last Name
- Customer Phone number

As par of Invoice table:
- Order id
- Total invoice to be paid



