from typing import Optional, Any
from pydantic import BaseModel
from database import Base
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey,Float


class Menus(Base):
    __tablename__ = 'menu_list'

    menu_id = Column(Integer, primary_key=True, index=True)
    menu_name = Column(String, unique = True)
    menu_descriptn = Column(String)
    menu_price = Column(Float)
    spicy = Column(Boolean, default=True)
    is_veg = Column(Boolean, default=True)
    menu_type = Column(String)


class Orders(Base):
    __tablename__ = 'placing_orders'
    order_no = Column(Integer, primary_key=True,index=True)
    order_id = Column(Integer)
    menu_name = Column(String, ForeignKey("menu_list.menu_name"))
    order_quantitiy = Column(Integer)
    customer_first_name = Column(String)
    customer_last_name = Column(String)
    contact_phone_no = Column(String)
    

class Invoice(Base):
    __tablename__ = 'bill_calculation'
    invoice_no = Column(String, primary_key=True)
    order_id = Column(Integer, ForeignKey("placing_orders.order_id"))
    total_cost = Column(Float)