from typing import Annotated,List
from fastapi import FastAPI, Body
from typing import Optional, Any
from pydantic import BaseModel
from database import SessionLocal
from models import Menus, Orders,Invoice
from sqlalchemy.orm import Session
from fastapi import Depends,HTTPException
import models
from database import engine
from starlette import status

app = FastAPI()

models.Base.metadata.create_all(bind=engine)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

db_dependency = Annotated[Session, Depends(get_db)]

session: Session = Depends(get_db)


class MenuList(BaseModel):
    menu_id: int
    menu_name: str 
    menu_descriptn: str
    menu_price: float
    spicy: bool
    is_veg: bool
    menu_type: str

class OrderPlacing(BaseModel):
    order_no: int
    order_id: int
    menu_name: str
    order_quantitiy: int
    customer_first_name: str
    customer_last_name: str
    contact_phone_no: str
    

class OrderUpdating(BaseModel):
    order_id: int
    menu_name: str
    order_quantitiy: int
    customer_first_name: str
    customer_last_name: str
    contact_phone_no: str

    class Config:
        orm_mode = True

class InvoiceGeneration(BaseModel):
    # invoice_no: int
    order_id: int
    total_price: float


def get_bill_by_id(invoice_no: int, db: db_dependency ):
    
    """
    Returns the menu based on menu_id
    """

    invoice_info = db.query(Menus).get(invoice_no)
    print(invoice_info)
    return invoice_info


def get_menu_info_by_id(menu_id: int, db: db_dependency ):
    
    """
    Returns the menu based on menu_id
    """

    menu_info = db.query(Menus).get(menu_id)
    return menu_info

def get_order_by_id(order_id: int, db: db_dependency):

    order_info = db.query(Orders).get(order_id)
    return order_info

def get_customer_details(phone_no: str, db: db_dependency):

    order_info = db.query(Orders).filter(Orders.contact_phone_no == phone_no).first()
    print(f'{order_info.customer_first_name}')
    return order_info.customer_first_name


def update_menu_update(menu_info: MenuList, updatedmenu: MenuList):

    """
    Update the Menu list based on the Menu changes
    """
    menu_info.menu_name = updatedmenu.menu_name
    menu_info.menu_id = updatedmenu.menu_id
    menu_info.menu_descriptn = updatedmenu.menu_descriptn
    menu_info.menu_price = updatedmenu.menu_price
    menu_info.spicy = updatedmenu.spicy
    menu_info.is_veg = updatedmenu.is_veg
    menu_info.menu_type = updatedmenu.menu_type
    return menu_info

def update_orders(order_info: OrderPlacing, updatedorder: OrderPlacing):

    order_info.order_id = updatedorder.order_id
    order_info.order_quantitiy = updatedorder.order_quantitiy
    order_info.menu_name = updatedorder.menu_name
    order_info.total_price = order_info.menu_price*order_info.order_quantitiy

    return order_info



@app.post('/create_menu_list/',status_code=status.HTTP_201_CREATED)
def create_menu_list(menuresponse: MenuList, db: db_dependency):
    """
    This method is able to create new menus onto the Database

    """
    menu_list = Menus(**menuresponse.dict())
    db.add(menu_list)
    db.commit()
    return(menuresponse)

@app.get('/get_menus_list')
def get_all_menus(db: db_dependency):

    """
    This method is able to get all the menus from the Database

    """
    return db.query(Menus).filter(Menus.menu_id).all()


@app.get('/get_customer_info/by_phone/{phone_no}')
def get_customer_details(phone_no: str, db: db_dependency):

    order_info = db.query(Orders).filter(Orders.contact_phone_no == phone_no).first()
    print(f'{order_info.customer_first_name}')
    return {"firstname" :order_info.customer_first_name,
            "lastname": order_info.customer_last_name }


@app.get('/get_menu_by_id/{menuid}')
def get_menu_by_id(menuid:int, db:db_dependency):
    
    """
    This method is able to get the specific menus from the Database

    """
    fetch_menu = db.query(Menus).filter(Menus.menu_id == menuid).all()
    if not fetch_menu:
        raise HTTPException(status_code=404, detail='Menu not found.')
    
    return db.query(Menus).filter(Menus.menu_id == menuid).all()


@app.put('/update/{menu_id}',status_code=status.HTTP_201_CREATED)
def update_menu(menu_id: int, updatedmenu: MenuList,db: db_dependency):

    """
    This method is able to get update the menus onto the Database

    """
    menu_info = get_menu_info_by_id(menu_id,db)
    print("value")
    print(type(menu_info))
    # menu_info = db.query(Menus).get(menu_id)
    if not menu_info:
        raise HTTPException(status_code=404, detail='Menu not yet available in list to amend the details.')

    menu_update = update_menu_update(menu_info, updatedmenu)
    db.add(menu_update)
    db.commit()
    return(updatedmenu)

@app.delete('/delete_menu/{menu_id}', status_code = status.HTTP_204_NO_CONTENT)
def menu_delete(menu_id: int, db: db_dependency):
    """
    This method is able to delete the menus from the Database

    """
    db.delete(db.query(Menus).get(menu_id))
    db.commit()
    return(db.query(Menus).filter(Menus.menu_id).all())


@app.post('/order_items/',status_code=status.HTTP_201_CREATED)
def orderitems(orderresponse: OrderPlacing, db: db_dependency):
    """
    This method creates the order and calculates the price for each order

    """

    order_list = Orders(**orderresponse.model_dump())
    print(dir(db))
    db.add(order_list)
    db.commit()
    return(orderresponse)


@app.get('/get_all_orders')
def getallorders(db: db_dependency):

    """
    This method is able to get all the orders from the Database

    """
    return db.query(Orders).filter(Orders.order_id).all()

@app.get('/get_by_order_id/{order_id}')
def get_orders_by_id(order_id: int, db: db_dependency):

    """
    Returns the order based on the order id
    """
    order_info =[]
    order_info = db.query(Orders).filter(Orders.order_id == order_id).all()
    if len(order_info) == 0 :
        raise HTTPException(status_code=404, detail='Specific order has not been created')
    return order_info


@app.put('/updateorder/{order_no}',status_code=status.HTTP_201_CREATED)
def updateorders(order_no: int, updatedorder: OrderUpdating,db: db_dependency):

    """
    This method is able to get update the orders into the Database

    """
    order_info = get_order_by_id(order_no,db)
    print(order_info.order_no)
    print(order_info.menu_name)
    if order_info.menu_name == updatedorder.menu_name:

        # menu_info = db.query(Menus).get(menu_id)
        if not order_info:
            raise HTTPException(status_code=404, detail='Order has not been placed already to amend the orders.')

        order_update = update_orders(order_info, updatedorder)
        print(dir(db))
        db.add(order_update)

        db.commit()
        return(updatedorder)

    else:
        raise HTTPException(status_code=404, detail="order not placed already")

@app.delete('/delete_the_order/{order_id}', status_code = status.HTTP_204_NO_CONTENT)
def delete_the_order_by_id(order_id: int, db: db_dependency):

    """
    This end point will delete the order based on order id
    """

    db.delete(db.query(Orders).get(order_id))
    db.commit()

    return { "message" : f"{order_id} is deleted"}


@app.get('/final_bill_check/',status_code=status.HTTP_201_CREATED)
def final_bill_check(order_id:int , db: db_dependency):
    """
    This method creates the order and calculates the price for each order

    """

    invoice_info = []
    invoice_info = db.query(Orders).filter(Orders.order_id == order_id).all()
    total_cost = 0
    final_bill = 0
    for item in invoice_info:
        menu, orderquantity = item.menu_name, item.order_quantitiy
        # print(f"{menu} -> {orderquantity}")
        # Menuprice = db.query(Menus).filter(Menus.menu_name == menu).first()
        # print(f"Menu --> {menu}")
        Menuprice = db.query(Menus).filter(Menus.menu_name == menu).all()

        for menuitem in Menuprice:
            print(f" {menuitem.menu_name} --> {menuitem.menu_price }")
            total_cost = total_cost + menuitem.menu_price * orderquantity
    # print(total_cost)

    return {
        "order_id" : order_id,
        "total_amount" : total_cost
    }